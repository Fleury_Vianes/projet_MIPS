-------------------------------------------------------------------------------
--! @file   operative/memory.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Data memory
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;
use ieee.std_logic_textio.all;

use work.types.all;

-------------------------------------------------------------------------------
--! Entity of data memory
entity memory is
	generic (
		DATA_FILE : string         ; --! Data file
		START     : address        ; --!
		SIZE      : integer       ); --! Memory size
	port(
		addr      : in  address    ; --! Reading / writing address
		data_r    : out word       ; --! Data read
		data_w    : in  word       ; --! Data written
		en        : in  std_logic  ; --! Enable read and write
		we        : in  std_logic  ; --! '0': read mode / '1': write mode
		valid     : out std_logic  ; --! Error Out Of Memory
		clk       : in  std_logic ); --! Clock input
end entity;

--! Procedural architecture of the data memory.
architecture procedural of memory is
	type data_type is array (0 to SIZE-1) of byte;

	impure function Initialize_memory(file_name : in string) return data_type is
	file     data_file  : text is in file_name;
	variable line_input : line;
	variable data       : data_type;
	begin

		for I in data_type'range loop
			if endfile(data_file) then -- Initaliser le reste de la memoire à 0
				data(I) := (others => '0');
			else
				readline(data_file, line_input);
				hread(line_input,   data(I));
			end if;
		end loop;

		if  not endfile(data_file) then
			report "Memory to small to load '" & file_name & "' (SIZE=" & integer'image(SIZE) & ")";
		end if;

		return data;
	end function;


	signal data : data_type := Initialize_memory(DATA_FILE);
	signal address : integer;
begin

	address <= to_integer(resize(signed(addr) - signed(START), 32));

	process(clk, address)
	begin
		if rising_edge(clk) then
			if(en = '0') then
				valid <= '0';
			elsif ( address<0 or address>SIZE-1 ) then
				valid <= '0'; -- error Out Of Memory
			else
				valid <= '1';
				if we = '0' then -- read
					data_r( 31 downto 24 ) <= data(address + 0);
					data_r( 23 downto 16 ) <= data(address + 1);
					data_r( 15 downto  8 ) <= data(address + 2);
					data_r(  7 downto  0 ) <= data(address + 3);
				else -- write
					data(address + 0) <= data_w( 31 downto 24 );
					data(address + 1) <= data_w( 23 downto 16 );
					data(address + 2) <= data_w( 15 downto  8 );
					data(address + 3) <= data_w( 7  downto  0 );
				end if;
			end if;
		end if;
	end process;

end architecture;

