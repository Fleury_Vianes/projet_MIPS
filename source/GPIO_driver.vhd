-------------------------------------------------------------------------------
--! @file   operative/memory.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Data memory
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;
use ieee.std_logic_textio.all;

use work.types.all;

-------------------------------------------------------------------------------
--! Entity of data memory
entity GPIO_driver is
	generic (
		IN_START  : address        ;
		IN_SIZE   : integer        ;
		OUT_START : address        ;
		OUT_SIZE  : integer       );
	port(
		gpio_out  : out std_logic_vector(OUT_SIZE - 1 downto 0);
		gpio_in   : in  std_logic_vector(IN_SIZE  - 1 downto 0);

		addr      : in  address    ; --! Reading / writing address
		data_r    : out word       ; --! Data read
		data_w    : in  word       ; --! Data written
		en        : in  std_logic  ; --! Enable read and write
		we        : in  std_logic  ; --! '0': read mode / '1': write mode
		valid     : out std_logic  ; --! Valid data
		clk       : in  std_logic  ; --! Clock
		rst       : in  std_logic ); --! Reset
end entity;

architecture procedural of GPIO_driver is
	constant data_in_end : integer := (IN_SIZE+31)/32 - 1;
	type data_in_type is array (0 to data_in_end) of word;
	signal data_in     : data_in_type := (OTHERS => (others => '0'));
	signal address_in  : integer;

	constant data_out_end : integer := (OUT_SIZE + 31)/32 - 1;
	type data_out_type is array (0 to data_out_end) of word;
	signal data_out    : data_out_type;
	signal address_out : integer;

--	signal data_in  : std_logic_vector(((IN_SIZE  + 31)/32)*32 - 1 downto 0);
--	signal data_out : std_logic_vector(((OUT_SIZE + 31)/32)*32 - 1 downto 0);
	signal valid_in, valid_out : std_logic;
begin

	valid <= valid_in or valid_out;

	-- Input driver
	address_in  <= to_integer(resize(signed(addr) - signed(IN_START), 32));
	FOR_IN : for I in 0 to data_in_end-1 generate
		data_in(I) <= gpio_in(I*32 + 31 downto I*32);
	end generate;
	data_in(data_in_end)((IN_SIZE - 1) mod 32 downto 0) <= gpio_in(data_in_end*32 + ( (IN_SIZE-1) mod 32) downto data_in_end*32);

	DRIVER_IN :
	process(clk, rst, address_in)
	begin
		if rst = '1' then
			data_r <= (others => '0');
			valid_in <= '0';
		elsif rising_edge(clk) then
			if(en = '0') then
				valid_in <= '0';
			elsif ( address_in < 0 or address_in/32 > IN_SIZE-1 ) then
				valid_in <= '0'; -- error Out Of Memory
			elsif we = '0' then -- read
				valid_in <= '1'; -- No error
				data_r <= data_in( address_in/32 );
			else
				valid_in <= '0';
			end if;
		end if;
	end process;

	-- Output driver
	address_out <= to_integer(resize(signed(addr) - signed(OUT_START), 32));

	FOR_OUT : for I in 0 to data_out_end-1 generate
		gpio_out(I*32+31 downto I*32) <= data_out(I);
	end generate;
	gpio_out((OUT_SIZE-1) mod 32 + data_out_end*32 downto data_out_end*32) <= data_out(data_out_end)((OUT_SIZE-1) mod 32 downto 0);

	DRIVER_OUT :
	process(clk, rst, address_out)
	begin
		if rst = '1' then
			data_out <= (others => (others => '0'));
			valid_out <= '0';
		elsif rising_edge(clk) then
			if(en = '0') then
				valid_out <= '0';
			elsif ( address_out<0 or address_out/32 > OUT_SIZE-1 ) then
				valid_out <= '0'; -- error Out Of Memory
			elsif we = '1' then -- write
				valid_out <= '1'; -- No error
				data_out(address_out) <= data_w;
			else
				valid_out <= '0';
			end if;
		end if;
	end process;

end architecture;

