library ieee;
use ieee.std_logic_1164.all;

use work.types.all;

entity top is
	port (
		switches : in  std_logic_vector(3 downto 0);
		leds     : out std_logic_vector(3 downto 0);
		clock : in  std_logic  ;
		reset : in  std_logic );
end entity;

architecture structural of top is
	component mips is
	port (
		mem_prog_addr : out address    ;
		mem_prog_en   : out std_logic  ;
		mem_prog_we   : out std_logic  ;
		mem_prog_out  : in  word       ;

		mem_data_addr : out address    ;
		mem_data_en   : out std_logic  ;
		mem_data_we   : out std_logic  ;
		mem_data_out  : in  word       ;
		mem_data_in   : out word       ;

		clk : in std_logic             ;
		rst : in std_logic            );
	end component;

	component memory is
	generic (
		SIZE      : integer        ;
		START     : address        ;
		DATA_FILE : string        );
	port(
		addr      : in  address    ;
		data_r    : out word       ;
		data_w    : in  word       ;
		en        : in  std_logic  ;
		we        : in  std_logic  ;
		valid   : out std_logic  ;
		clk       : in  std_logic );
	end component;

	component GPIO_driver is generic (
		IN_START  : address        ;
		IN_SIZE   : integer        ;
		OUT_START : address        ; --!
		OUT_SIZE  : integer       );
	port(
		gpio_out  : out std_logic_vector(OUT_SIZE - 1 downto 0);
		gpio_in   : in  std_logic_vector(IN_SIZE  - 1 downto 0);

		addr      : in  address    ; --! Reading / writing address
		data_r    : out word       ; --! Data read
		data_w    : in  word       ; --! Data written
		en        : in  std_logic  ; --! Enable read and write
		we        : in  std_logic  ; --! '0': read mode / '1': write mode
		valid     : out std_logic  ; --! Error Out Of Memory
		clk       : in  std_logic  ; --! CLock
		rst       : in  std_logic ); --! Reset
	end component;

	component memory_bus_solver is
	generic(
		INPUT    : integer := 1   ;
		BUS_SIZE : integer := 32 );
	port(
		data_in  : in  std_logic_vector( INPUT*BUS_SIZE -1 downto 0 )  ;
		validity : in  std_logic_vector( INPUT          -1 downto 0 )  ;
		data_out : out std_logic_vector(       BUS_SIZE -1 downto 0 ) );
	end component;



	signal mem_data_addr  : address   ;
	signal mem_data_in    : word      ;
	signal mem_data_out   : word      ;
	signal mem_data_en    : std_logic ;
	signal mem_data_we    : std_logic ;
	signal mem_data_valid : std_logic ;

	signal mem_bus_data     : std_logic_vector(63 downto 0) ;
	signal mem_bus_validity : std_logic_vector( 1 downto 0) ;
	signal mem_bus_data_out : word;

	signal mem_prog_addr  : address   ;
	signal mem_prog_out   : word      ;
	signal mem_prog_en    : std_logic ;
	signal mem_prog_we    : std_logic ;
	signal mem_prog_valid : std_logic ;
begin

	CPU : mips
	port map (
		mem_data_addr => mem_data_addr ,
		mem_data_out  => mem_bus_data_out  ,
		mem_data_in   => mem_data_in   ,
		mem_data_en   => mem_data_en   ,
		mem_data_we   => mem_data_we   ,

		mem_prog_addr => mem_prog_addr ,
		mem_prog_out  => mem_prog_out  ,
		mem_prog_en   => mem_prog_en   ,
		mem_prog_we   => mem_prog_we   ,

		clk => clock,
		rst => reset
	);

	MEM_PROG : memory  generic map (SIZE => 64, START => X"00000000",
	                                DATA_FILE => "data/out/prog.text.hex")
	port map (
		addr   => mem_prog_addr,
		data_r => mem_prog_out,
		data_w => X"00000000",
		we     => mem_prog_we,
		en     => mem_prog_en,
		clk    => clock
	);


	MEM_DATA : memory  generic map (SIZE => 32, START => X"08000000",
	                                DATA_FILE => "data/out/prog.data.hex")
	port map (
		addr   => mem_data_addr,
		data_w => mem_data_in,
		data_r => mem_bus_data(31 downto 0),
		valid  => mem_bus_validity(0),
		we     => mem_data_we,
		en     => mem_data_en,
		clk    => clock
	);


	GPIO1 : GPIO_driver
	generic map (
		IN_START  => X"00200000" ,
		IN_SIZE   => 4           ,
		OUT_START => X"00100000" ,
		OUT_SIZE  => 4          )
	port map (
		gpio_in  => switches,
		gpio_out => leds,

		addr   => mem_data_addr  ,
		data_w => mem_data_in    ,
		data_r => mem_bus_data(63 downto 32),
		valid  => mem_bus_validity(1) ,
		en     => mem_data_en    ,
		we     => mem_data_we    ,
		clk => clock  ,
		rst => reset );

	BUS_SOLVER:
	memory_bus_solver
	generic map ( INPUT => 2 )
	port map (
		data_in  => mem_bus_data,
		validity => mem_bus_validity,
		data_out => mem_bus_data_out
	);

end architecture;

