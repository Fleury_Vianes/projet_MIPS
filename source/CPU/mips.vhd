------------------------------------------------------------------------------
--! @file   mips.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Top design of the MIPS cpu
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;

package mips_types is

	type control is record
		-- mux ---------------------------------------------------------------
		mux_1 : std_logic_vector(0 downto 0) ; --! Mux 1 selector
		mux_2 : std_logic_vector(1 downto 0) ; --! Mux 2 selector
		mux_3 : std_logic_vector(0 downto 0) ; --! Mux 3 selector
		mux_4 : std_logic_vector(1 downto 0) ; --! Mux 4 selector
		mux_5 : std_logic_vector(1 downto 0) ; --! Mux 5 selector

		-- alu ---------------------------------------------------------------
		alu_op : alu_operation               ; --! Alu operations

		-- register bank -----------------------------------------------------
		regbank_rd_en : std_logic            ; --! Register rd write

		-- memory ------------------------------------------------------------
		mem_instr_we : std_logic             ; --! Write en of program memory
		mem_instr_en : std_logic             ; --! Enable program memory
		mem_data_we  : std_logic             ; --! Write en of data memory
		mem_data_en  : std_logic             ; --! Enable data memory

		-- pc ----------------------------------------------------------------
		pc_en : std_logic                    ; --! PC register write
	end record control;

	type feedback is record
		-- operation ---------------------------------------------------------
		funct  : funct_type  ;
		opcode : opcode_type ;

		-- alu ---------------------------------------------------------------
		alu_o : std_logic ;
		alu_z : std_logic ;
		alu_s : std_logic ;

		-- memory ------------------------------------------------------------
	--	data_err_OOM  : std_logic ; -- unused
	--	instr_err_OOM : std_logic ; -- unused
	end record feedback;

end package;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;
use work.mips_types.all;

-------------------------------------------------------------------------------
--! Entity of the MIPS CPU
entity mips is
	port (
		mem_prog_addr : out address    ;
		mem_prog_en   : out std_logic  ;
		mem_prog_we   : out std_logic  ;
		mem_prog_out  : in  word       ;

		mem_data_addr : out address    ;
		mem_data_en   : out std_logic  ;
		mem_data_we   : out std_logic  ;
		mem_data_out  : in  word       ;
		mem_data_in   : out word       ;

		clk : in std_logic             ;
		rst : in std_logic            );
end entity;

architecture top of mips is

	component control_unit is
	port (
		control  : out control    ;
		feedback : in  feedback   ;
		clk, rst : in  std_logic );
	end component;

	component operative_unit is
	port (
		control      : in  control    ; --! Command from Control Unit
		feedback     : out feedback   ; --! Feedback for Control Unit
		pc           : out address    ;
		instr        : in  word       ;
		data_address : out address    ;
		data_in      : in  word       ;
		data_out     : out word       ;
		clk, rst     : in  std_logic );
	end component;


	--
	signal control_s  : control;
	signal feedback_s : feedback;
begin

	PC : control_unit
	port map (
		control  => control_s,
		feedback => feedback_s,
		clk      => clk,
		rst      => rst
	);

	PO : operative_unit
	port map (
		control  => control_s,
		feedback => feedback_s,

		pc    => mem_prog_addr,
		instr => mem_prog_out,

		data_address => mem_data_addr,
		data_out     => mem_data_in,
		data_in      => mem_data_out,

		clk      => clk,
		rst      => rst
	);

	mem_data_en <= control_s.mem_data_en;
	mem_data_we <= control_s.mem_data_we;

	mem_prog_we <= control_s.mem_instr_we;
	mem_prog_en <= control_s.mem_instr_en;

end architecture;

