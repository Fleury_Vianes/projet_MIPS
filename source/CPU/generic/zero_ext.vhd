-------------------------------------------------------------------------------
--! @file   generic/zero_ext.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Generic register
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
--! Entity of zero ext
--! 1 inputs/1 output
--! Generics : SIZE_IN: Size of input data SIZE_OUT: Size of output data
--! Inputs  : data
--! Outputs : data zero-extended
entity zero_ext is
	generic (SIZE_IN, SIZE_OUT : integer);
	port (
		data_in  : in  std_logic_vector( SIZE_IN  - 1 downto 0 )  ;
		data_out : out std_logic_vector( SIZE_OUT - 1 downto 0 ) );
end entity;

architecture dataflow of zero_ext is
begin

	data_out <= std_logic_vector( resize(unsigned(data_in), data_out'length) );

end architecture;
