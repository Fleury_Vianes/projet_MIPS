-------------------------------------------------------------------------------
--! @file   generic/sign_ext.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Sign Extension
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
--! Entity of sign ext
--! 1 inputs/1 output
--! Generics : SIZE_IN: Size of input data SIZE_OUT: Size of output data
--! Inputs  : data
--! Outputs : data sign-extended
entity sign_ext is
	generic (SIZE_IN, SIZE_OUT : integer);
	port (
		data_in  : in  std_logic_vector( SIZE_IN  - 1 downto 0 )  ;
		data_out : out std_logic_vector( SIZE_OUT - 1 downto 0 ) );
end entity;

architecture dataflow of sign_ext is
begin

	data_out <= std_logic_vector( resize(signed(data_in), data_out'length) );

end architecture;
