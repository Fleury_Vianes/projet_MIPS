-------------------------------------------------------
--! @file   generic/mux.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Generic mux
-------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


--! @brief Entity of a generic multiplexer
entity mux is
	generic (
		N        : integer  ;	--! data size
		SEL_SIZE : integer ); --! selection size
	port (
		data_in  : in  std_logic_vector ( N * (2 ** SEL_SIZE) -1 downto 0 )  ; --! data input
		data_out : out std_logic_vector ( N                   -1 downto 0 )  ; --! data output
		sel      : in  std_logic_vector (           SEL_SIZE  -1 downto 0 ) ); --! Input selection
end entity;

architecture Behavioral of mux is
	signal index : integer;
begin

	process (data_in, sel, index)
	begin
		index <= to_integer( unsigned(sel) );
		if ((to_integer( unsigned(sel)) >= 2 ** SEL_SIZE) or (to_integer( unsigned(sel)) < 0)) then
			data_out <= (others => '0');
		else
			data_out <= data_in(( to_integer( unsigned(sel)) +1 )*N - 1 downto to_integer( unsigned(sel))*N);
		end if;
	end process;

end architecture;
