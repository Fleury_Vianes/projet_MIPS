-------------------------------------------------------
--! @file   generic/reg.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Generic register
-------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Entity of a register
--! 4 inputs/1 output
--! Generics : N : Size of data
--! Inputs  : data + reset + clock + load/enable
--! Outputs : data memorized
-------------------------------------------------------------------------------
entity reg is
	generic (SIZE: integer);
	port (
		o   : out std_logic_vector(SIZE-1 downto 0) ; --! data output
		i   : in  std_logic_vector(SIZE-1 downto 0) ; --! data input
		en  : in  std_logic                         ; --! enable write
		rst : in  std_logic                         ; --! async. reset
		clk : in  std_logic                        ); --! clock
end entity;

architecture description of reg is
begin

	process(clk, rst)
	begin
		if rst = '1' then
			o <= (others=>'0');
		elsif rising_edge(clk) and en = '1' then
			o <= i;
		end if; -- < register: o
	end process;

end architecture;
