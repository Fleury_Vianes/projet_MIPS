library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--library mips;
use work.types.all;
use work.mips_types.all;

entity control_unit is
	port (
		control  : out control    ; --! Control for datapath
		feedback : in  feedback   ; --! Feedback from datapath
		clk, rst : in  std_logic ); --! Clock and reset input
end entity;

architecture RTL of control_unit is
	type state is (
		INITIALIZE,
		DECODE,
		OPERATION,
		OPERATION_IMM,
		SHIFT,
		SHIFT_VARIABLE,
		BRANCH,
		JUMP,
		JUMP_REGISTER,
		STORE_LOAD,
		WRITE_MEMORY,
		READ_MEMORY,
		LOAD_UPPER_IMM,
		FETCH_INSTRUCTION,
		BAD_INSTRUCTION
	);

	signal current_state, next_state : state;
	signal opcode, funct: integer;
begin

	-- Memorisation de l'etat
	process (clk, rst)
	begin

		if rst='1' then
			current_state <= INITIALIZE;
		elsif rising_edge(clk) then
			current_state <= next_state;
		end if; -- < flip flop pour current_state

	end process;


	process (current_state, feedback, opcode, funct)
	begin
		opcode <= to_integer( unsigned(feedback.opcode) );
		funct  <= to_integer( unsigned(feedback.funct) );

		case current_state is
			when DECODE =>
				case opcode is
					when 0 => -- FUNCT
						case funct is
							when 2#000000# to 2#000111# => next_state <= SHIFT;           -- Shift
							when 2#001000#              => next_state <= JUMP_REGISTER;   -- JR  Jump Register
							when 2#001001#              => next_state <= BAD_INSTRUCTION; -- JUMP_LINK_REGISTER;
							when 2#010000# to 2#010011# => next_state <= BAD_INSTRUCTION; -- Move HI and LO
							when 2#011000# to 2#011011# => next_state <= BAD_INSTRUCTION; -- DIV and MULT
							when 2#100000# to 2#100011# => next_state <= OPERATION;       -- Operation Arithmetic
							when 2#100100# to 2#100111# => next_state <= OPERATION;       -- Operation Logic
							when 2#101010# to 2#101011# => next_state <= BAD_INSTRUCTION; -- SLT, SLTU
							when others                 => next_state <= BAD_INSTRUCTION;
						end case;
					when 2#000001#              => next_state <= BAD_INSTRUCTION; --
					when 2#000010#              => next_state <= JUMP;            -- J   Jump
					when 2#000011#              => next_state <= BAD_INSTRUCTION; -- JAL Jump And Link
					when 2#000100# to 2#000101# => next_state <= BRANCH;          -- BEQ, BNE
					when 2#000110# to 2#000111# => next_state <= BAD_INSTRUCTION; -- BLEZ, BGTZ
					when 2#001000# to 2#001110# => next_state <= OPERATION_IMM;   -- Imediate operations
					when 2#001111#              => next_state <= LOAD_UPPER_IMM;  --
					-- Load instructions
					when 2#100000# to 2#100010# => next_state <= BAD_INSTRUCTION; -- Not implemented
					when 2#100011#              => next_state <= STORE_LOAD;
					when 2#100100# to 2#100110# => next_state <= BAD_INSTRUCTION; -- Not implemented
					-- Store instructions
					when 2#101000# to 2#101010# => next_state <= BAD_INSTRUCTION; -- Not implemented
					when 2#101011#              => next_state <= STORE_LOAD;
					when 2#101110#              => next_state <= BAD_INSTRUCTION; -- not implemented
					-- OTHERS
					when others                 => next_state <= BAD_INSTRUCTION;
				end case;
			when STORE_LOAD   =>
				if (feedback.opcode(3) = '0') then next_state <= READ_MEMORY;
				else                               next_state <= WRITE_MEMORY;
				end if;
			when WRITE_MEMORY    => next_state <= FETCH_INSTRUCTION;
			when READ_MEMORY     => next_state <= FETCH_INSTRUCTION;
			when JUMP            => next_state <= FETCH_INSTRUCTION;
			when JUMP_REGISTER   => next_state <= FETCH_INSTRUCTION;
			when BRANCH          => next_state <= FETCH_INSTRUCTION;
			when OPERATION_IMM   => next_state <= FETCH_INSTRUCTION;
			when OPERATION       => next_state <= FETCH_INSTRUCTION;
			when LOAD_UPPER_IMM  => next_state <= FETCH_INSTRUCTION;
			when SHIFT           => next_state <= FETCH_INSTRUCTION;
			when BAD_INSTRUCTION => next_state <= FETCH_INSTRUCTION;
			when FETCH_INSTRUCTION         => next_state <= DECODE;
			when others          => next_state <= DECODE;
		end case;

	end process;


	process (current_state, feedback)
	begin

		case current_state is
			when DECODE => control <= (
				mux_1 => "-", mux_2 => "--", mux_3 => "-", mux_4 => "--", mux_5 => "--",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '0',
				pc_en => '0'
			);
			when OPERATION => control <= (
				mux_1 => "0", mux_2 => "00", mux_3 => "0", mux_4 => "00", mux_5 => "-0",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => "0" & feedback.funct(2 downto 0),
				regbank_rd_en => '1',
				pc_en => '1'
			);
			when OPERATION_IMM => control <= (
				mux_1 => "1", mux_2 => "1" & feedback.opcode(2), mux_3 => "0", mux_4 => "00", mux_5 => "-0",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => "0" & feedback.opcode(2 downto 0),
				regbank_rd_en => '1',
				pc_en => '1'
			);
			when FETCH_INSTRUCTION |
			     INITIALIZE => control <= (
				mux_1 => "-", mux_2 => "--", mux_3 => "-", mux_4 => "--", mux_5 => "--",
				mem_instr_en => '1', mem_instr_we => '0',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '0',
				pc_en => '0'
			);
			when BRANCH => control <= (
				mux_1 => "-", mux_2 => "00", mux_3 => "-", mux_4 => "0"&(feedback.alu_z xor feedback.opcode(0)), mux_5 => "-0",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_SUB,
				regbank_rd_en => '0',
				pc_en => '1'
			);
			when JUMP => control <= (
				mux_1 => "-", mux_2 => "--", mux_3 => "-", mux_4 => "10", mux_5 => "--",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '0',
				pc_en => '1'
			);
			when JUMP_REGISTER => control <= (
				mux_1 => "-", mux_2 => "--", mux_3 => "-", mux_4 => "11", mux_5 => "--",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '0',
				pc_en => '1'
			);
			when READ_MEMORY => control <= (
				mux_1 => "1", mux_2 => "--", mux_3 => "1", mux_4 => "00", mux_5 => "--",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '1',
				pc_en => '1'
			);
			when WRITE_MEMORY => control <= (
				mux_1 => "0", mux_2 => "--", mux_3 => "-", mux_4 => "00", mux_5 => "--",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '0',
				pc_en => '1'
			);
			when STORE_LOAD => control <= (
				mux_1 => "-", mux_2 => "10", mux_3 => "-", mux_4 => "--", mux_5 => "-0",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '1', mem_data_we  => feedback.opcode(3),
				alu_op   => ALU_ADD,
				regbank_rd_en => '0',
				pc_en => '0'
			);
			when SHIFT_VARIABLE => control <= (
				mux_1 => "0", mux_2 => "00", mux_3 => "0", mux_4 => "00", mux_5 => "11",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => "10" & feedback.funct(1 downto 0),
				regbank_rd_en => '1',
				pc_en => '1'
			);
			when SHIFT => control <= (
				mux_1 => "0", mux_2 => "00", mux_3 => "0", mux_4 => "00", mux_5 => "01",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => "10" & feedback.funct(1 downto 0),
				regbank_rd_en => '1',
				pc_en => '1'
			);
			when LOAD_UPPER_IMM => control <= (
				mux_1 => "1", mux_2 => "11", mux_3 => "0", mux_4 => "00", mux_5 => "11",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_SLL,
				regbank_rd_en => '1',
				pc_en => '1'
			);

			-- OTHERS
			when others => control <= (
				mux_1 => "-", mux_2 => "--", mux_3 => "-", mux_4 => "00", mux_5 => "--",
				mem_instr_en => '0', mem_instr_we => '-',
				mem_data_en  => '0', mem_data_we  => '-',
				alu_op   => ALU_NOOP,
				regbank_rd_en => '0',
				pc_en => '1'
			);
		end case;
	end process;

end architecture;

