--! @file   operative_int.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Operative Unit for MIPS cpu

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mips_types.all;
use work.types.all;

entity operative_unit is
	port (
		control      : in  control    ; --! Command from Control Unit
		feedback     : out feedback   ; --! Feedback for Control Unit
		pc           : out address    ;
		instr        : in  word       ;
		data_address : out address    ;
		data_in      : in  word       ;
		data_out     : out word       ;
		clk, rst     : in  std_logic );
end entity;

architecture structural of operative_unit is
	-- Component Instanciation

	-- Generic Components
	component reg is
		generic (SIZE: integer);
		port (
			o   : out std_logic_vector(SIZE-1 downto 0) ;
			i   : in  std_logic_vector(SIZE-1 downto 0) ;
			en  : in  std_logic                         ;
			rst : in  std_logic                         ;
			clk : in  std_logic                        );
	end component;

	component mux is
		generic (
			N        : integer  ;
			SEL_SIZE : integer );
		port (
			data_in  : in  std_logic_vector ( N * (2 ** SEL_SIZE) -1 downto 0 )  ;
			data_out : out std_logic_vector ( N                   -1 downto 0 )  ;
			sel      : in  std_logic_vector (           SEL_SIZE  -1 downto 0 ) );
	end component;

	component sign_ext is
		generic (SIZE_IN, SIZE_OUT : integer);
		port (
			data_in  : in  std_logic_vector( SIZE_IN  - 1 downto 0 )  ;
			data_out : out std_logic_vector( SIZE_OUT - 1 downto 0 ) );
	end component;

	component zero_ext is
		generic (SIZE_IN, SIZE_OUT : integer);
		port (
			data_in  : in  std_logic_vector( SIZE_IN  - 1 downto 0 )  ;
			data_out : out std_logic_vector( SIZE_OUT - 1 downto 0 ) );
	end component;

	component addr_ext is
		port (
			pc       : in  address                        ;
			addr_in  : in  std_logic_vector(25 downto 0)  ;
			addr_out : out address                       );
	end component;


	--   component instruction_decoder is
	--   end component;
	--
	component register_bank is
		Port (
			data_rs : out word := (others => '0') ;
			data_rt : out word := (others => '0') ;
			data_rd : in  word                    ;
			sel_rs  : in  reg_type                ;
			sel_rt  : in  reg_type                ;
			sel_rd  : in  reg_type                ;
			en_w    : in  std_logic               ;
			clk     : in  std_logic               ;
			rst     : in  std_logic              );
	end component;

	component instruction_decoder is
		port (
			instr  : in  word;
			opcode : out opcode_type                    ;
			rs     : out reg_type                       ;
			rt     : out reg_type                       ;
			rd     : out reg_type                       ;
			IMM    : out half_word                      ;
			addr   : out std_logic_vector(25 downto 0)  ;
			shamt  : out std_logic_vector( 4 downto 0)  ;
			funct  : out funct_type                    );
	end component;

	component alu is
		port (
			op : in  alu_operation  ;
			A  : in  word           ; -- input A
			B  : in  word           ; -- input B
			R  : out word           ; -- result
			o  : out std_logic      ; -- operation overflow ?
			z  : out std_logic      ; -- result is zero ?
			s  : out std_logic     ); -- result sign ?
	end component;

	component add is
		Port (
			A : in  word  ; -- input A
			B : in  word  ; -- input B
			R : out word ); -- result
	end component;

	---- Signal Declaration

	signal opcode : opcode_type;
	signal funct  : funct_type;
	signal rs, rt, rd, rd_sel : reg_type;
	signal shamt_data : std_logic_vector(4 downto 0);
	signal IMM_data   : half_word;
	signal addr_data  : std_logic_vector(25 downto 0);

	signal IMM_data_signext, IMM_data_zeroext : word;
	signal mem_data_out : word;

	-- register bank
	signal rs_data : word ;
	signal rt_data : word ;
	signal rd_data : word ;

	signal shamt   : word ;

	-- ALU
	signal alu_A   : word ;
	signal alu_B   : word ;
	signal alu_R   : word ;

	-- Constants
	constant word_zero : word := (others => '0') ; --! Value (unsigned)  0
	constant word_16   : word := X"00000010"     ; --! Value (unsigned) 16
	constant word_4    : word := X"00000004"     ; --! Value (unsigned)  4

	-- PC
	signal pc_current,    pc_next, pc_branch,
	       pc_next_instr, pc_jump, pc_offset  : address;

	signal mux_1_in : std_logic_vector(  5*2 - 1 downto 0 );
	signal mux_2_in : std_logic_vector( 32*4 - 1 downto 0 );
	signal mux_3_in : std_logic_vector( 32*2 - 1 downto 0 );
	signal mux_4_in : std_logic_vector( 32*4 - 1 downto 0 );
	signal mux_5_in : std_logic_vector( 32*4 - 1 downto 0 );
begin


	pc <= pc_current;
	data_address <= alu_r;
	data_out     <= rt_data;
	mem_data_out <= data_in;

	feedback.funct  <= funct  ;
	feedback.opcode <= opcode ;

	REG_PC : reg
		generic map (SIZE => 32)
		port map (
			i   => pc_next        ,
			o   => pc_current     ,
			en  => control.pc_en  ,
			clk => clk            ,
			rst => rst           );

	DECODER : instruction_decoder
	port map (
		instr  => instr  ,
		opcode => opcode ,
		funct  => funct  ,
		rs => rs, rt => rt, rd => rd,
		shamt => shamt_data  ,
		IMM   => IMM_data    ,
		addr  => addr_data  );

	ADD_PC : add
	port map (
		A => pc_current     ,
		B => word_4         ,
		R => pc_next_instr );

	pc_offset <= std_logic_vector(signed(IMM_data_signext) sll 2);
	ADD_BRANCH : add
	port map (
		A => pc_next_instr  ,
		B => pc_offset      ,
		R => pc_branch     );

	SIGN_EXT_1 : sign_ext  generic map (SIZE_IN => 16, SIZE_OUT => 32)
	port map (data_in => IMM_data, data_out => IMM_data_signext);

	ZERO_EXT_1 : sign_ext  generic map (SIZE_IN => 16, SIZE_OUT => 32)
	port map (data_in => IMM_data, data_out => IMM_data_zeroext);

	ZERO_EXT_2 : sign_ext  generic map (SIZE_IN =>  5, SIZE_OUT => 32)
	port map (data_in => shamt_data, data_out => shamt);

	ADDR_EXT_1 : addr_ext
	port map (
		addr_in => addr_data,
		pc => pc_current,
		addr_out => pc_jump
	);

	REG_BANK : register_bank
	port map (
		sel_rs => rs,
		sel_rt => rt,
		sel_rd => rd_sel,
		data_rs => rs_data,
		data_rt => rt_data,
		data_rd => rd_data,
		en_w => control.regbank_rd_en,
		clk => clk, rst => rst
	);

	ALU1 : alu
	port map (
		A  => alu_A,
		B  => alu_B,
		R  => alu_R,
		op => control.alu_op,
		o  => feedback.alu_o,
		s  => feedback.alu_s,
		z  => feedback.alu_z
	);

	--           1    0
	mux_1_in <= rt & rd;
	MUX1 : mux  generic map (N => 5, SEL_SIZE => 1)
	port map (
		data_in  => mux_1_in,
		data_out => rd_sel,
		sel => control.mux_1
	);
	--                         3                  2           1         0
	mux_2_in <= IMM_data_zeroext & IMM_data_signext & word_zero & rt_data;
	MUX2 : mux  generic map (N => 32, SEL_SIZE => 2)
	port map (
		data_in  => mux_2_in,
		data_out => alu_B,
		sel => control.mux_2
	);
	--                     1       0
	mux_3_in <= mem_data_out & alu_R;
	MUX3 : mux  generic map (N => 32, SEL_SIZE => 1)
	port map (
		data_in  => mux_3_in,
		data_out => rd_data,
		sel => control.mux_3
	);
	--                3           2             1                 0
	mux_4_in <= rs_data  &  pc_jump  &  pc_branch  &  pc_next_instr;
	MUX4 : mux  generic map (N => 32, SEL_SIZE => 2)
	port map (
		data_in  => mux_4_in,
		data_out => pc_next,
		sel => control.mux_4
	);

	--                 3          2         1           0
	mux_5_in <=  word_16 &  rs_data  &  shamt  &  rs_data;
	MUX5 : mux  generic map (N => 32, SEL_SIZE => 2)
	port map (
		data_in  => mux_5_in,
		data_out => alu_A,
		sel => control.mux_5
	);

end architecture;

