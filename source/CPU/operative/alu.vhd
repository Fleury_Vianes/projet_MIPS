-------------------------------------------------------------------------------
--! @file
--! @author Arthur Vianes - Rodolphe Fleury
--! @brief ALU (Arithmetic and Logic Unit)
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;

-------------------------------------------------------------------------------
--! Entity of ALU
--! Invalid operations result in null value
entity alu is
	port (
		op : in  alu_operation  ; --! Operation
		A  : in  word           ; --! Input A
		B  : in  word           ; --! Input B
		R  : out word           ; --! Result (A op B)
		o  : out std_logic      ; --! Result is Overflow ?
		z  : out std_logic      ; --! Result is Zero ?
		s  : out std_logic     ); --! Result Sign ?
end entity;

--! Behavorial architecture of the ALU
architecture Behavioral of ALU is
	signal result, A_s, B_s : signed(word'left + 1 downto 0);
	constant word_zero      : signed(word'left + 1 downto 0) := (others => '0');
begin

	A_s <= resize(signed(A), result'length);
	B_s <= resize(signed(B), result'length);

	process (A_s, B_s, op, result)
	begin
		case op is
			-- Arithmetique operations -----------------
			when ALU_ADD  => result <=  A_s  +  B_s    ;
			when ALU_SUB  => result <=  A_s  -  B_s    ;

			-- Logic operations ------------------------
			-- Note : logic op never overflow
			when ALU_AND  => result <=  A_s and B_s    ;
			when ALU_OR   => result <=  A_s or  B_s    ;
			when ALU_XOR  => result <=  A_s xor B_s    ;
			when ALU_NOR  => result <=  A_s nor B_s    ;

			-- Shift -----------------------------------
			when ALU_SLL  => result <=         shift_left (          B_s , to_integer(unsigned(A_s(4 downto 0))) );
			when ALU_SRL  => result <=  signed(shift_right( unsigned(B_s), to_integer(unsigned(A_s(4 downto 0))) ));
			when ALU_SRA  => result <=         shift_right(          B_s , to_integer(A_s(4 downto 0)) );

			-- Invalid operations ----------------------
			when others   => result <= (others => '0') ;
		end case;

		-- Results ----------------------------------------
		if (std_match(result, word_zero)) then z <= '1';
		else z <= '0';
		end if;
	--	z <= '1' when (std_match(result, word_zero)) else '0';
		o <=                   result(word'LEFT + 1     )  ;
		s <=                   result(word'LEFT         )  ;
		R <= std_logic_vector( result(word'LEFT downto 0) );

	end process;

end architecture;

