-------------------------------------------------------
--! @file
--! @author Arthur Vianes - Rodolphe Fleury
--! @brief Simple Adder
-------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

use work.types.all;

-------------------------------------------------------------------------------
--! Entity of adder
--! 2 inputs/1 output
--! Inputs  : Operands
--! Outputs : Result of the addition
entity add is
	Port (
		A : in  word  ; -- input A
		B : in  word  ; -- input B
		R : out word ); -- result
end entity;

--! dataflow architecture of adder
architecture dataflow of add is
begin

	R <= std_logic_vector(unsigned(A) + unsigned(B)) ;

end architecture;
