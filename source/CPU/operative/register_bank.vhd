-------------------------------------------------------
--! @file   operative/register_bank.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Register bank
-------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

use work.types.ALL;

-------------------------------------------------------------------------------
--! Entity of bank register
--! 7 inputs/2 outputs
--! Inputs  : Part of instruction code
--! Outputs : data contained in instruction code
entity register_bank is
	Port (
		data_rs : out word       ;
		data_rt : out word       ;
		data_rd : in  word       ;
		sel_rs  : in  reg_type   ;
		sel_rt  : in  reg_type   ;
		sel_rd  : in  reg_type   ;
		en_w    : in  std_logic  ;
		clk     : in  std_logic  ;
		rst     : in  std_logic );
end entity;

architecture Behavioral of register_bank is
	type bank_type is array (1 to 2 ** sel_rs'LENGTH-1) of word;
	signal bank : bank_type;
	signal rs_addr, rt_addr, rd_addr : integer;
begin

	rs_addr <= to_integer(unsigned( sel_rs )) ;
	rt_addr <= to_integer(unsigned( sel_rt )) ;
	rd_addr <= to_integer(unsigned( sel_rd )) ;

	process (clk, rst, rs_addr, rt_addr, rd_addr)
	begin
		-- async reset
		if rst='1' then
			bank <= (others => (others => '0'));
			data_rs <= (others => '0');
			data_rt <= (others => '0');
		elsif rising_edge(clk) then
			-- Select Register Source
			case rs_addr is
				when 0      => data_rs <= (others => '0'); -- zero register
				when others => data_rs <= bank(rs_addr);
			end case;

			-- Select Register Target
			case rt_addr is
				when 0      => data_rt <= (others => '0'); -- zero register
				when others => data_rt <= bank(rt_addr);
			end case;

			if en_w='1' then
				-- Select Register Destination
				case rd_addr is
					when 0      =>  -- zero register
					when others => bank(rd_addr) <= data_rd;
				end case;
			end if;
		end if;

	end process;

end architecture;

