library ieee;
use ieee.std_logic_1164.all;

use work.types.all;

entity addr_ext is
	port (
		pc       : in  address                        ;
		addr_in  : in  std_logic_vector(25 downto 0)  ;
		addr_out : out address                       );
end entity;

architecture dataflow of addr_ext is
begin

	addr_out( 1 downto  0) <= "00";
	addr_out(27 downto  2) <= addr_in;
	addr_out(31 downto 28) <= PC(31 downto 28);

end architecture;
