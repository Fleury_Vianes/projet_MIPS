-------------------------------------------------------------------------------
--! @file   operative/instruction_decoder.vhd
--! @author Arthur Vianes, Rodolphe Fleury
--! @brief  Instruction decoder
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.types.all;

-------------------------------------------------------------------------------
--! Entity of instruction decoder
--! 1 input/8 outputs
--! Inputs  : Instruction code
--! Outputs : all the part of the instruction code
entity instruction_decoder is
	port (
		instr  : in  word;
		opcode : out opcode_type                    ;
		rs     : out reg_type                       ;
		rt     : out reg_type                       ;
		rd     : out reg_type                       ;
		IMM    : out half_word                      ;
		addr   : out std_logic_vector(25 downto 0)  ;
		shamt  : out std_logic_vector( 4 downto 0)  ;
		funct  : out funct_type                    );
end entity;

--! Dataflow architecture of instruction decoder
architecture Dataflow of instruction_decoder is
begin
--  Data   ---------    Range     ------ Formats -- Size
	opcode <= instr( 31 downto 26 ) ; -- I J R   --  6
	rs     <= instr( 25 downto 21 ) ; -- I   R   --  5
	rt     <= instr( 20 downto 16 ) ; -- I   R   --  5
	rd     <= instr( 15 downto 11 ) ; --     R   --  5
	shamt  <= instr( 10 downto  6 ) ; --     R   --  5
	funct  <= instr(  5 downto  0 ) ; --     R   --  6
	addr   <= instr( 25 downto  0 ) ; --   J     -- 26
	IMM    <= instr( 15 downto  0 ) ; -- I       -- 16
end architecture;
