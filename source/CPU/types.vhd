-------------------------------------------------------
--! @file
--! @author Arthur Vianes - Rodolphe Fleury
--! @brief Personnal defined types
-------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! @brief Ensemble des types de notre architecture
package types is

	-- Base types -----------------------------------
	subtype byte        is std_logic_vector( 7 downto 0) ; --!  8 bits data
	subtype half_word   is std_logic_vector(15 downto 0) ; --! 16 bits data
	subtype word        is std_logic_vector(31 downto 0) ; --! 32 bits data
	subtype address     is word                          ; --! 32 bits address

	-- Instructions types ---------------------------
	subtype reg_type    is std_logic_vector( 4 downto 0) ; --! register id
	subtype opcode_type is std_logic_vector( 5 downto 0) ; --! opcode data
	subtype funct_type  is std_logic_vector( 5 downto 0) ; --! funct data

--	-- ALU types ------------------------------------
	subtype alu_operation is std_logic_vector(3 downto 0);
	constant ALU_NOOP : alu_operation := "----"; --! NO OPeration
	constant ALU_ADD  : alu_operation := "0000"; --! Arithmetic Add
	constant ALU_ADDU : alu_operation := "0001"; --! Arithmetic Unsigned ADD
	constant ALU_SUB  : alu_operation := "0010"; --! Arithmetic Sub
	constant ALU_SUBU : alu_operation := "0011"; --! Arithmetic Unsigned SUB
	constant ALU_AND  : alu_operation := "0100"; --! Logic AND
	constant ALU_OR   : alu_operation := "0101"; --! Logic OR
	constant ALU_XOR  : alu_operation := "0110"; --! Logic XOR
	constant ALU_NOR  : alu_operation := "0111"; --! Logic NOR
	constant ALU_SLL  : alu_operation := "1000"; --! Shift Left Logic
	constant ALU_SRL  : alu_operation := "1010"; --! Shift Right Logic
	constant ALU_SRA  : alu_operation := "1011"; --! Shift Right Arithmetic

end package;

