library ieee;
use ieee.std_logic_1164.all;


entity memory_bus_solver is
	generic(
		INPUT    : integer := 1   ;
		BUS_SIZE : integer := 32 );
	port(
		data_in  : in  std_logic_vector( INPUT*BUS_SIZE -1 downto 0 )  ;
		validity : in  std_logic_vector( INPUT          -1 downto 0 )  ;
	data_out : out std_logic_vector(       BUS_SIZE -1 downto 0 ) );
end entity;


architecture dataflow of memory_bus_solver is
begin


	process(validity, data_in)
		variable tmp : std_logic_vector( BUS_SIZE-1 downto 0 );
	begin
		tmp := (others => '0');
		for i in validity'range loop
			if validity(i) = '1' then
				tmp := tmp or data_in( (i+1)*BUS_SIZE - 1 downto i*BUS_SIZE );
			end if;
		end loop;
		data_out <= tmp;

	end process;


end architecture;
