#Compilateur VHDL + param
CVHDL=ghdl
FVHDL=--ieee=synopsys

#Générateur de trace simu
GTRACE=
FTRACE=

#Simulateur
SIMU=gtkwave
FSIMU=
TIME?=10ns

SRC= $(shell find source -name "*.vhd")
BNCH= $(shell find bench -name "*.vhd")
TOP?=top

all: trace

analyse: $(SRC) $(BNCH)
	@echo "\033[41m               Analyse of .vhd files              \033[0m"
	@$(CVHDL) -i $(FVHDL) $(SRC)
	@$(CVHDL) -a -g $(FVHDL)
	@if [ $$? -eq 0 ]; then echo "\033[36mAnalysing source OK!\033[0m"; fi;
	@$(CVHDL) -i $(FVHDL) $(BNCH)
	@$(CVHDL) -a -g $(FVHDL)
	@if [ $$? -eq 0 ]; then echo "\033[36mAnalysing bench OK!\033[0m"; fi;

build: analyse $(SRC) $(BNCH)
	@echo "\033[42m              Elaboration of top file             \033[0m"
	@$(CVHDL) -m --ieee=synopsys $(TOP)
	@if [ $$? -eq 0 ]; then echo "\033[36mBuild OK!\033[0m"; fi;

run: build
	@echo "\033[45m     Run the design and generate a trace file     \033[0m"
	@$(CVHDL) -r --ieee=synopsys $(TOP) --stop-time=$(TIME) --wave=$(TOP).ghw

trace: run
	@echo "\033[44m    Launch gtkwave in order to visualize files     \033[0m"
	@gtkwave $(TOP).ghw --end=$(TIME)
	@if [ $$? -eq 0 ]; then echo "\033[36mRun finish\033[0m"; fi;

doc:
	@goxygen .doxygen

doc_clean:
	@rm -rf doc/latex doc/html

clean:
	@$(CVHDL) --clean

remove: clean doc_clean
	@$(CVHDL) --remove
	@rm -f *.vcd *.ghw

rebuild: remove build

