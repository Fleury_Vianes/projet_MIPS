
.data
D1:     .word   0x00000F00
D2:     .word   0x00FF00FF
D3:     .word   0x0000FFFF
D4:     .word   0xFFFF0000
D5:     .word   0x00000000
D6:     .word   0xFFFFFFFF
D7:     .word   0xAAAA0000

.origin 0x00100000
INPUT:

.origin 0x00200000
OUTPUT:


# Program entry
.text
.globl __start
__start:
        j       main


# Main program
main:

loop:
        lw      $a1, 0x00200000

        andi    $a2, $a1, 0x3

        srl     $a1, $a1, 2
        andi    $a3, $a1, 0x3

        add     $a1, $a2, $a1

        sw      $a1, 0x00100000

        j       loop

