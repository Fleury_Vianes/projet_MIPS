
# Dependance
`gcc-mips-linux-gnu`  Compilateur/Linker/Assembleur pour architecture MIPS
`vim-common`          necessaire pour l'outil xxd
`binutils`            Pour les outils d'analyse  de fichier ELF (Il y a aussi
                      un package specialement pour mips Mais il n'est pas
                      necssaire ici)

## Ubuntu - Debian
`sudo apt-get install gcc-mips-linux-gnu vim-common binutils`

