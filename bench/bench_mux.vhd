library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bench_mux is
end entity;

architecture bench of bench_mux is
	component mux is
		generic (
		N        : integer  ;
		SEL_SIZE : integer );
		Port (
		data_in  : in  std_logic_vector ( N * (2 ** SEL_SIZE)-1 downto 0 )  ;
		data_out : out std_logic_vector ( N                  -1 downto 0 )  ;
		sel      : in  std_logic_vector (           SEL_SIZE -1 downto 0 ) );
	end component;

	signal s_data_out : std_logic_vector ( 7 downto 0 ) ;
	signal s_sel      : unsigned ( 1 downto 0 ) := "00";

	signal clock : std_logic := '0';
	begin

		DUT : mux generic map (N => 8, SEL_SIZE => 2)
		--         sel |  3 "11"  |  2 "10"  |  1 "01"  |  0 "00"  |
		port    map (
			data_in  => "00001111"&"00111100"&"11110000"&"10101010" ,
			data_out => s_data_out  ,
			sel      => std_logic_vector(s_sel)      );

		clock <= not clock after 1 ns;

		process (clock)
		begin
			if rising_edge(clock) then
				if s_sel = "11" then s_sel <= "00";
				else                 s_sel <= s_sel + 1;
				end if;
			end if;
		end process;

	end architecture;
