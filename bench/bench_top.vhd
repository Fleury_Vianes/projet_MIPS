library ieee;
use ieee.std_logic_1164.all;

entity bench_top is
end entity;

architecture tb of bench_top is
	component top is
		port (
		switches : in  std_logic_vector(3 downto 0);
		leds     : out std_logic_vector(3 downto 0);
		clock, reset : in std_logic );
	end component;

	signal gpio_in  : std_logic_vector(3 downto 0);
	signal gpio_out : std_logic_vector(3 downto 0);
	signal clock, reset : std_logic := '1';
begin
	reset <= '0'       after 1.5 ns;
	clock <= not clock after 1   ns;

	DUT : top
	port map (
		switches => gpio_in  ,
		leds     => gpio_out ,
		clock => clock       ,
		reset => reset      );

	gpio_in <= "1011";

end architecture;

