library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;

entity bench_alu is
end entity;

architecture tb of bench_alu is
	component  alu is
		port (
			op : in  alu_operation  ;
			A  : in  word           ; -- input A
			B  : in  word           ; -- input B
			R  : out word           ; -- result
			o  : out std_logic      ; -- operation overflow ?
			z  : out std_logic      ; -- result is zero ?
			s  : out std_logic     ); -- result sign ?
	end component;

	signal R : word;
	signal o, z, s : std_logic;

	signal clk  : std_logic := '0';
	signal A, B : word := (others => '0');
	signal op   : alu_operation;
begin

--	clk <= not clk after 1 ns;

	process
	begin
		A <= X"00000000";
		B <= X"00000000";
		op <= OP_SUB;
		wait for 1 ns;
		A <= X"00000001";
		B <= X"00000002";

		wait for 1 ns;

		B <= (others => '0');
		wait for 1 ns;
	end process;

	DUT : alu
	port map(
		op => op,
		A  => A,
		B  => B,
		R  => R,
		o  => o,
		z  => z,
		s  => s
	);

end architecture;
