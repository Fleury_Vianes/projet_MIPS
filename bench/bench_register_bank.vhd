library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.types.all;

entity bench_register_bank is
end entity;

architecture test of bench_register_bank is
	component register_bank port (
		data_rs : out word       ;
		data_rt : out word       ;
		data_rd : in  word       ;
		sel_rs  : in  reg_type   ;
		sel_rt  : in  reg_type   ;
		sel_rd  : in  reg_type   ;
		en_w    : in  std_logic  ;
		clk     : in  std_logic  ;
		rst     : in  std_logic );
	end component;

	type data_test_type is array (0 to 3) of word;
	constant data_test : data_test_type := (
		"01101001000011110101101011110000"  ,
		"11111111111111111111111111111111"  ,
		"11111111111110000000000111111111"  ,
		"00000000000001111111111000000000" );

	signal s_sel_rs : reg_type := "00000" ;
	signal s_sel_rt : reg_type := "00001" ;
	signal s_sel_rd : reg_type := "00001" ;

	signal s_data_rd : word := data_test(0);
	signal s_data_rs, s_data_rt : word;

	signal s_en_w: std_logic := '0';
	signal clock, reset : std_logic := '1';

begin

	BANK1: register_bank port map (
	data_rs => s_data_rs ,
	data_rt => s_data_rt ,
	data_rd => s_data_rd ,
	sel_rs => s_sel_rs   ,
	sel_rt => s_sel_rt   ,
	sel_rd => s_sel_rd   ,
	en_w   => s_en_w     ,
	clk    => clock      ,
	rst    => reset     );

	clock <= not clock after 1 ns;
	reset <= '0' after 2 ns;

	process
	begin
		wait for 3.5 ns;
		s_en_w <= '1';

		wait for 1 ns;
		-- s_en_w <= '0';
		s_data_rd <= data_test(1);

		wait for 1 ns;
		s_sel_rd <= "00010";

		wait for 1 ns;
		s_sel_rd <= "00000";

		wait for 2 ns;
		s_en_w <= '0';
		s_data_rd <= data_test(3);
		s_sel_rd <= "11111";

		wait for 2 ns;
		s_en_w <= '1';

	end process;

end architecture;
