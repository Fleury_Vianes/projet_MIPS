library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;

entity bench_addr_ext is
end entity;

architecture tb of bench_addr_ext is
	component addr_ext is
		Port (
		PC       : in  address                ;
		addr_in  : in  std_logic_vector(25 downto 0)  ;
		addr_out : out address               );
	end component;

	signal PC_s       : address               := (others => '0') ;
	signal addr_in_s  : std_logic_vector(25 downto 0) := (others => '0') ;
	signal addr_out_s : address                                  ;
begin

	process
	begin
		PC_s <= (others => '0');
		addr_in_s <= (0 => '1', others => '0');
		wait for 1 ns;
		PC_s(31 downto 21) <= "01010101010";
		wait for 1 ns;

		addr_in_s <= (0 => '0', others => '1');
		wait for 1 ns;
	end process;

	dut : addr_ext
	port map (
		pc => PC_s,
		addr_in  => addr_in_s,
		addr_out => addr_out_s
	);

end architecture;
