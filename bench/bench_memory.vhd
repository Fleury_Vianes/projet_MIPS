library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;

entity bench_memory is
end entity;

architecture tb of bench_memory is
	-- Instanciation du composant
	component memory is
	generic (MEM_SIZE : integer);
	port(
		addr     : in address;
		rw       : in std_logic;
		en       : in  std_logic;
		clk      : in std_logic;
		rst      : in std_logic;
		data_w   : in word;
		err_oom  : out std_logic;
		data_r   : out word
	);
	end component;

	--Décalaration des signaux
	signal addr_s    : address := (others => '0');
	signal rw_s      : std_logic := '0';
	signal clk_s     : std_logic := '0';
	signal rst_s     : std_logic := '1';
	signal data_w_s  : word  := (others => '0');
	signal err_oom_s : std_logic := '0';
	signal data_r_s  : word ;
begin
-- Design Unit Test
	dut : memory
	generic map (MEM_SIZE => 32)
	port map(
		addr    => addr_s,
		rw      => rw_s,
		en      => '1',
		clk     => clk_s,
		rst     => rst_s,
		data_w  => data_w_s,
		err_oom => err_oom_s,
		data_r  => data_r_s
	);
-- Génération des stimulis
	clk_s <= not(clk_s) after 1 ns;

	process
	begin
		-- W puis read à la même adresse
		wait for 0.5 ns;
		addr_s    <= std_logic_vector(to_unsigned(01, addr_s'length));
		rst_s     <= '0'; -- read
		data_w_s  <= (0 => '1' , others => '0'); -- Data writing
		wait for 4 ns;
		rw_s      <= '1'; -- write
		wait for 4 ns;
		rw_s      <= '0'; -- Data outputted
		-- W à une adresse puis read à une autre adress
		wait for 20 ns;
		rw_s      <= '1'; -- write
		addr_s    <= std_logic_vector(to_unsigned(02, addr_s'length));
		rst_s     <= '0'; -- read
		data_w_s  <= (1 => '1' , others => '0'); -- Data writing
		wait for 4 ns;
		rw_s      <= '0' ;-- Data outputted
		addr_s    <= std_logic_vector(to_unsigned(03, addr_s'length));
		-- W/R à une adresse hors des limites
		wait for 20 ns;
		rw_s      <= '1'; -- write
		addr_s    <= std_logic_vector(to_unsigned(32, addr_s'length));
		rst_s     <= '0';
		data_w_s  <= (2 => '1' , others => '0'); -- Data writing
		wait for 4 ns;
		rw_s      <= '0' ; -- read
		-- Test reset asynchrone
		-- wait for 20 ns;
		rst_s     <= '1'; -- reset
		-- wait for 4 ns;
		rw_s      <= '0'; -- read
		addr_s    <= std_logic_vector(to_unsigned(01, addr_s'length));
		wait for 4 ns;
		addr_s    <= std_logic_vector(to_unsigned(02, addr_s'length));
	end process;
end architecture;
