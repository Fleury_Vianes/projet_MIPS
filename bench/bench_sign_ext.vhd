library ieee;
use ieee.std_logic_1164.all;

entity bench_sign_ext is
end entity;

architecture tb of bench_sign_ext is
	component sign_ext is
		generic (SIZE_IN, SIZE_OUT : integer);
		port (
			data_in  : in  std_logic_vector( SIZE_IN  - 1 downto 0 )  ;
			data_out : out std_logic_vector( SIZE_OUT - 1 downto 0 ) );
	end component;

	signal data_in_s  : std_logic_vector( 7 downto 0) := (others => '0');
	signal data_out_s : std_logic_vector(15 downto 0);
begin

	process
	begin
		data_in_s <= "01000000";
		wait for 1 ns;

		data_in_s <= "10000000";
		wait for 1 ns;
	end process;

	SIGN_EXT1 : sign_ext
		generic map (
			SIZE_IN  =>  8,
			SIZE_OUT => 16 )
		port    map (
			data_in  => data_in_s,
			data_out => data_out_s );

end architecture;
