library ieee;
use ieee.std_logic_1164.all;

use work.types.all;

entity bench_GPIO_driver is
end entity;

architecture tb of bench_GPIO_driver is

	component GPIO_driver
	generic (
		IN_START  : address        ;
		IN_SIZE   : integer        ;
		OUT_START : address        ;
		OUT_SIZE  : integer       );
	port(
		gpio_out  : out std_logic_vector(OUT_SIZE - 1 downto 0);
		gpio_in   : in  std_logic_vector(IN_SIZE  - 1 downto 0);

		addr      : in  address    ; --! Reading / writing address
		data_r    : out word       ; --! Data read
		data_w    : in  word       ; --! Data written
		en        : in  std_logic  ; --! Enable read and write
		we        : in  std_logic  ; --! '0': read mode / '1': write mode
		valid     : out std_logic  ; --! Error Out Of Memory
		clk       : in  std_logic  ; --! Clock
		rst       : in  std_logic ); --! Reset_driver is
	end component;

	signal GPIO_out_s : std_logic_vector( 4-1 downto 0);
	signal GPIO_in_s  : std_logic_vector( 6-1 downto 0);

	signal addr_s : address := (others => '0');
	signal data_r_s, data_w_s : word;
	signal en_s, we_s, valid_s: std_logic;

	signal clk : std_logic := '1';
	signal rst : std_logic := '1';

begin

	rst <= '0'     after   1 ns;
	clk <= not clk after 0.5 ns;

	process
	begin
		GPIO_in_s <= "100101";
		addr_s <= X"00100000";
		en_s <= '0';
		we_s <= '0';
		data_w_s <= X"00000002";
		wait for 0.5 ns;
		wait for 1 ns;
		addr_s <= X"00100000";
		en_s <= '1';
		wait for 1 ns;
		en_s <= '0';
		wait for 1 ns;
		addr_s <= X"00200000";
		we_s <= '1';
		en_s <= '1';
		wait for 1 ns;
		en_s <= '0';
		wait for 1 ns;


	end process;

	DUT1 : GPIO_driver generic map (
		IN_START  => X"00100000" ,
		IN_SIZE   => 6           ,
		OUT_START => X"00200000" ,
		OUT_SIZE  => 4           )
	port map (
		gpio_out => GPIO_out_s , -- GPIO to drive (output)
		gpio_in  => GPIO_in_s  , -- GPIO to drive (input)
		addr    => addr_s   , -- Memory bus
		data_r  => data_r_s , --
		data_w  => data_w_s , --
		en      => en_s     , --
		we      => we_s     , --
		valid   => valid_s  , --
		clk    => clk   , -- clock reset control
		rst    => rst  ); --

end architecture;

