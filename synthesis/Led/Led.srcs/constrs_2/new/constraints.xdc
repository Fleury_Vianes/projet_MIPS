create_clock -period 1000.000 -name CLK -waveform {0.000 500.000} -add [get_ports -filter { NAME =~  "*" && DIRECTION == "IN" }]

set_property PACKAGE_PIN H5 [get_ports {LED[3]}]
    set_property IOSTANDARD LVCMOS18 [get_ports {LED[3]}]
set_property PACKAGE_PIN J5 [get_ports {LED[2]}]
    set_property IOSTANDARD LVCMOS18 [get_ports {LED[2]}]
set_property PACKAGE_PIN T9 [get_ports {LED[1]}]
    set_property IOSTANDARD LVCMOS18 [get_ports {LED[1]}]
set_property PACKAGE_PIN T10 [get_ports {LED[0]}]
    set_property IOSTANDARD LVCMOS18 [get_ports {LED[0]}]
set_property PACKAGE_PIN E3 [get_ports CLK]
    set_property IOSTANDARD LVCMOS18 [get_ports {CLK}]


set_property BITSTREAM.CONFIG.CONFIGRATE 50 [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]
