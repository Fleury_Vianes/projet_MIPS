----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.03.2018 18:04:30
-- Design Name: 
-- Module Name: led - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity led_blink is
    Port ( CLK   : in STD_LOGIC;
           LED   : out STD_LOGIC_VECTOR(3 downto 0)
         );
end led_blink;

architecture Behavioral of led_blink is
    signal compteur : INTEGER;
    signal led_s    : STD_LOGIC := '0';
begin
process(CLK)
begin
    if (rising_edge(CLK)) then
        compteur <= compteur + 1;
        if(compteur >= 100_000_000) then
            led_s  <= not led_s;   
              
            LED(0) <= not led_s;
            LED(1) <= led_s;
            LED(2) <= not led_s;
            LED(3) <= led_s;
            compteur <= 0;
        end if;
    end if;
end process;
end Behavioral;
