create_clock -period 1.000 -name virtual_clock

set_property PACKAGE_PIN A8 [get_ports {I[0]}]
set_property PACKAGE_PIN C11 [get_ports {I[1]}]
set_property PACKAGE_PIN H5 [get_ports O]
set_property IOSTANDARD LVCMOS18 [get_ports {I[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {I[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports O]

set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]

