create_clock -period 100.000 -name clock -waveform {0.000 50.000} [get_ports clock]
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]
set_property IOSTANDARD LVCMOS18 [get_ports clock]
set_property IOSTANDARD LVCMOS18 [get_ports reset]
set_property PACKAGE_PIN E3 [get_ports clock]
set_property PACKAGE_PIN C2 [get_ports reset]

set_property PACKAGE_PIN H5 [get_ports {leds[3]}]
set_property PACKAGE_PIN J5 [get_ports {leds[2]}]
set_property PACKAGE_PIN T9 [get_ports {leds[1]}]
set_property PACKAGE_PIN T10 [get_ports {leds[0]}]
set_property PACKAGE_PIN A8 [get_ports {switches[3]}]
set_property PACKAGE_PIN C11 [get_ports {switches[2]}]
set_property PACKAGE_PIN C10 [get_ports {switches[1]}]
set_property PACKAGE_PIN A10 [get_ports {switches[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {switches[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {switches[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {switches[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {switches[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[0]}]

create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list clock_IBUF_BUFG]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 4 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {GPIO1/switches[3][0]} {GPIO1/switches[3][1]} {GPIO1/switches[3][2]} {GPIO1/switches[3][3]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 4 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {GPIO1/leds[3][0]} {GPIO1/leds[3][1]} {GPIO1/leds[3][2]} {GPIO1/leds[3][3]}]]


set_input_delay -clock [get_clocks *clock*] -min -add_delay 5.000 [get_ports -filter { NAME =~  "*switches*" && DIRECTION == "IN" }]
set_input_delay -clock [get_clocks *clock*] -max -add_delay 10.000 [get_ports -filter { NAME =~  "*switches*" && DIRECTION == "IN" }]
set_output_delay -clock [get_clocks *clock*] -min -add_delay 1.000 [get_ports -filter { NAME =~  "*leds*" && DIRECTION == "OUT" }]
set_output_delay -clock [get_clocks *clock*] -max -add_delay 2.000 [get_ports -filter { NAME =~  "*leds*" && DIRECTION == "OUT" }]

set_input_delay -clock [get_clocks *clock*] -min -add_delay 5.000 [get_ports -filter { NAME =~  "*reset*" && DIRECTION == "IN" }]
set_input_delay -clock [get_clocks *clock*] -max -add_delay 10.000 [get_ports -filter { NAME =~  "*reset*" && DIRECTION == "IN" }]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clock_IBUF_BUFG]
