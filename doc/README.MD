# Documentation
La documentation de ce projet a été écrite au format LaTeX. Tous les fichiers
concernant les différentes sections du document sont placé dans le dossier
`sections`, toutes les images utilisées sont dans le dossier `images`.

Pour build la documentation il est possible d'utiliser le fichier Makefile mais
il est indispensable avant cela de vérifier si tous les packets nécessaires
sont installés.
## Dépendance (Ubuntu/Debian)
Sous Ubuntu et Debian, les packets nécessaires lié a LaTeX sont:
- texlive              -  Pour générer la documentation
- texlive-latex-extra  -  Pour les modules les plus important de LaTeX
- python-pygments      -  Pour bien gérer la colorisation syntaxique
- texlive-lang-french  -  Pour que LaTeX puisse écrite le fichier en Français

Pour installés les packets nécessaire vous pouvez executer la commande:
```shell
sudo apt-get install texlive texlive-latex-extra python-pygments texlive-lang-french
```
